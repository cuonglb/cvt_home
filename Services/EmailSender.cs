﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CVT_Home.Services
{
    public class EmailSender:IEmailSender
    {
        private string _host = "smtp.gmail.com";
        private int _port = 587;
        private bool _enableSSL = true;
        private string _userName = "lybaocuong@gmail.com";
        private string _password = "kuztgojigqwryjhv";

        Task IEmailSender.SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SmtpClient(_host, _port)
            {
                Credentials = new NetworkCredential(_userName, _password),
                EnableSsl = _enableSSL
            };

            return client.SendMailAsync(new MailMessage(_userName, email, subject, htmlMessage) { IsBodyHtml = true });
        }
    }
}

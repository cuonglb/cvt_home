﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CVT_Home.Models;
using CVT_Home.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.ComponentModel.DataAnnotations;

namespace CVT_Home.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IEmailSender _email;
        public HomeController(ILogger<HomeController> logger, IEmailSender email)
        {
            _logger = logger;
            _email = email;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public bool ContactUs(string full_name, string email, string content)
        {
            if (new EmailAddressAttribute().IsValid(email))
            {
                string str = "Họ tên: " + full_name + "<br>";
                str += "Email: " + email + "<br>";
                str += "Nội dung: " + content + "<br>";

                _email.SendEmailAsync("lybaocuong@gmail.com", "Contact Us", str);
            }

            return true;
        }
    }
}
